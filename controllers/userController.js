const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registration = async (req, res) => {
  try {
    // Input validation
    const { email, firstName, lastName, password } = req.body;
    if (!email || !firstName || !lastName || !password) {
      return res
        .status(400)
        .json({ status: 400, message: "All fields are required." });
    }

    // Check email if already exists
    const existingUser = await User.findOne({ email: req.body.email });

    if (existingUser) {
      return res
        .status(400)
        .json({ status: 400, message: "Email already exists. Try again." });
    }

    // Hash password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Create new user
    const newUser = new User({
      email,
      firstName,
      lastName,
      password: hashedPassword,
    });

    // Save user to database
    await newUser.save();

    // Return success response with status 201 (Created)
    return res
      .status(201)
      .json({ status: 201, message: "User registered successfully." });
  } catch (error) {
    // Handle errors
    console.error("Registration error:", error);
    return res
      .status(500)
      .json({ status: 201, message: "Internal server error." });
  }
};

// User login
module.exports.login = async (req, res) => {
  try {
    // Find user by email
    const result = await User.findOne({ email: req.body.email });

    if (!result) {
      // If user not found
      return res.status(400).json({ status: 400, message: "User not found." });
    } else {
      // Check if password is correct
      const isPasswordCorrect = bcrypt.compareSync(
        req.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        // Create and send access token
        return res.status(201).json({
          status: 201,
          id: result._id,
          access: auth.createAccessToken(result),
        });
      } else {
        return res
          .status(400)
          .json({ status: 400, message: "Incorrect password. Try again." });
      }
    }
  } catch (error) {
    return res.status(500).json({ status: 500, message: error.message });
  }
};

module.exports.retrieveUserInfo = async (req, res) => {
  try {
    // Assuming you have the User model imported, you can access the user's ID from req.user
    const userId = req.user.id;

    // Use the user's ID to retrieve user details from the database
    const result = await User.findById(userId);

    // If no user found
    if (!result)
      return res.status(400).send({ status: 400, message: "User not found." });

    return res.status(201).send(result);
  } catch (error) {
    return res.status(500).send({ status: 500, message: error.message });
  }
};

module.exports.changeName = async (req, res) => {
  try {
    const userId = req.user.id;
    const { firstName, lastName } = req.body;

    const result = await User.findById(userId);

    if (!result) {
      return res.status(400).send({ status: 400, message: "User not found." });
    }

    const nameFields = {
      firstName: firstName,
      lastName: lastName,
    };

    const updateName = await User.findByIdAndUpdate(userId, nameFields);

    return res
      .status(201)
      .send({ status: 201, message: "Change name successfull" });
  } catch (error) {
    return res.status(500).send({ status: 500, message: error.message });
  }
};
