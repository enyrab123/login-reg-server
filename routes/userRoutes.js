const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");
const { verify } = auth;

// Routing component
const router = express.Router();

// Route for registration
router.post("/registration", userController.registration);

// Route for loginUser
router.post("/login", userController.login);

// Route for getUserDetails
router.get("/retrieve-user-info", verify, userController.retrieveUserInfo);

// Route for change name
router.put("/change-name", verify, userController.changeName);

module.exports = router;
